# JLV Test Automation

## Lisa Taylor Setup
The JLV development and operations team is building out an automated testing capability. Tools include Selenium, TestNG, and the Java programming language. Two department-of-veterans-affairs GitHub repositories have been established for this:

Repository | Description
---------- | -----------
jlv-cv-automation-framework | Reusable code not specifically tied to JLV
jlv-test-automation | JLV specific code

To get started, clone both repositories and load them both into your IDE, e.g., IntelliJ IDEA.  

When I start IntelliJ and see the opening dialog to reopen recent projects, it shows:
 - project: jlv-test-automation
 - location: C:\work\swdev\jlv-cv-automation-framework

 Selecting this opens both repositories. However, if I manually open jlv-test-automation from disk, it only opens this repo.

These two repositories use "main" as the main long-lived branch. For your work, create a feature branch off main and follow the standard GitFlow process for pull requests, reviews, and merging.

The Framework uses Maven to managed software dependencies, which are included in a POM file. IntelliJ is able to directly use the POM and other configuration files to build and run test scripts within the IDE. IntelliJ is also configured with plugins, including TestNG, which allows you to run tests directly from XML test suite files.

You will need a copy of the file, **Setup.zip** which contains various setup and configuration information. Refer to the top-level and folder-level README.txt files for information on where to copy the provided files, how to use them, and other technical details.

### Framework Components
Below is a listing of Test Automation Framework tools that comprise our test automation framework.

#### Java
The tools that comprise the Test Automation Framework require Java version 11 or higher. We recommend Amazon Corretto OpenJDK 11, which is freely available and easy to install. Refer to the Corretto website at: https://aws.amazon.com/corretto/.


#### Selenium
From www.selenium.dev
> Selenium WebDriver is a collection of language specific bindings to drive a browser. WebDriver drives a browser natively, as a user would, either locally or on a remote machine using the Selenium server. Selenium WebDriver refers to both the language bindings and the implementations of the individual browser controlling code. This is commonly referred to as just WebDriver. Selenium WebDriver is a W3C Recommendation.

All Selenium dependencies are pulled into your project via the pom.xml file that IntelliJ uses.

In addition to the Selenium WebDriver libriaries, you will need to install the individual browser-specifc WebDriver executables. Place these in folder of your choosing and update your path so that they can be found.

#### TestNG
We are using TestNG as the test runner for automated testing. This tool has many annotations that are critical to understand increating our test cases.

Make sure you are familiar with the creation and use of:
 - DataProviders, which are a means to pass data to test scriptst case
 - Suite xml file, which defines what data to use and which scripts (or @Test methods of a script) to run

**Important TestNG Annotations include:**
> @DataProvider  

> @Test
>  - dependsOnMethod
>  - priority
>  - dataProvider

#### AssertJ
Fluent Assertions Java Library

AssertJ provides a robust and flexible solution for verification points (or assertions) within our test scripts.

Try to use more meaningful checks whenever possible as opposed to doing the evaluation in your script and checking for a true/false value. By executing the check with AssertJ instead of in your code, AssertJ gives more detailed information on the failure, explaining what data was provided and what was expected.

If you do Bollean compares (isTrue() or isFalse()), you will only be left with a message that it failed without a real or meaningful explanation of why. There will be some cases where a Boolean check makes sense (such as checking if a button is disabled), but make sure that is not the default and you have considered otehr options.

**Different Types of Scripts**
> JlvTestScript (End to End Script) - These scripts will inherit the login and patient search methods for JLV, so you will not need to add additional code for those pieces.

> ScriptSnippet - This is a type of TestScript that is called from inside of an End to End TestScript or from a Module. Any assertions that are evaluated in a script snippet will be logged in the test that called it.

**Test Script Structure**
You can place your entire script in one method, but ideally it is better to split scripts into sections and have a @Test method for each section. This allows multiple benefits, as listed below.

**Benefits of multi-method scripts**
 - Easier to provide a DataProvider specific to the section of code. Using a Data Provider will allow a method to be executed any number of times with different data values. For example, a test method for drop downs can be run for different values and different drop downs.
 - Better logging is achieved because the results can easily show which data values were used, and you can quickly see which data caused failures, and which passed.
 - The ability to urn sections of a test script by defining the sute XML file will allow you to set up shorter tests that only run relevant portions of the test script for your current test scenario. That means if you need to test functionality covered in the full regression test, but do not wish to run the entire regression script, you can easily do so.

#### Xray and Jira REST APIs
Reporting is done by sending test execution results to Xray. To do this, we use both the Jira and Xray REST APIs. Xray Server is built on top of Jira; all its underlying objects are customized Jira issues.

We are using **Kong Unirest** over HTTP to send REST requests for Jira and Xray.

#### Apache POI
Apache POI is used to interact with Excel data files.

#### Apache log4j2
Apache log4j2 is used for debug logging.
 
#### IntelliJ IDEA
Add info about IntelliJ setup, including Java projects.

From Lisa:
> IntelliJ has a "template" feature that helps automate the creation of new testing scripts.

**Dependency Management**
The pom.xml file, along with Maven, are used to manage software dependencies. This should be available to you automatically as part of the two repositories that you clone and the use of IntelliJ IDE.

**POM Files**
Excerpt from https://maven.apache.org/guides/introduction/introduction-to-the-pom.html
> A Project Object Model or POM is the fundamental unit of work in Maven. It is an XML file that contains information about the project and configuration details used by Maven to build the project. It contains default values for most projects. Examples for this is the build directory, which is **`target`**; the source directory, which is **`src/main/java`**; the test source directory, which is **`src/test/java`**; and so on. When executing a task or goal, Maven looks for the POM in the current directory. It reads the POM, gets the needed configuration information, then executes the goal.

> Some of the configuration that can be specified in the POM are the project dependencies, the plugins or goals that can be executed, the build profiles, and so on. Other information such as the project version, description, developers, mailing lists and such can also be specified.

### Other Items in Word Doc
JlvTestScript

Writing a Test using Test Modules

These modules are listed in the XML, and can each have their own parameters.

Working with MainPage (the main portal window after logging in to JLV)

Working with Widgets

Working with Dialogs

TestNG

AssertJ

### Tool Installation and Setup

#### Java Installation
I'm using a portable installation of Corretto OpenJDK 11. Downloaded amazon-corretto-11.0.12.7.1-windows-x64-jdk.zip 
to downloads folder. Unzipped and copied resulting top level folder (and subfolders) to:
> C:\work\jdk11.0.12_7

Per JLV 3.0 software setup and configuration instructions, I made several updates to system environment variables.
> From the Windows start button, search for "environment variables"
> Select "Edit environment variables for your account"
> Ensure that User Variables for your account are set

I did not update the PATH variable. Instead, I created an environment variable that points to a scripts folder that I set up.
> CPS_SCRIPT = c:\work\swdev\script

Running this script updates the %PATH% environment variable for the current terminal session.
```bash
%CPS_SCRIPT%\setdev.bat
```

#### GitHub Repositories
**Cloning the GitHub Repos**
I used the following steps to clone `jlv-cv-automation-framework` and `jlv-test-automation` to my local machine.
```bash
cd c:\work\swdev\va
git clone https://<personal-access-token>@github.com/department-of-veterans-affairs/jlv-cv-automation-framework.git
git clone https://<personal-access-token>@github.com/department-of-veterans-affairs/jlv-test-automation.git
```

**Setting up a local branch**
The primary branch for these two repositories is "main". For my development work, I created a local branch as follows:

```bash
git branch cps-development
git checkout cps-development
```
### Lisa's Setup.zip file
#### Setup folder
As noted earlier, Lisa has prepared a zip file, **Setup.zip** with information, instructions, and supporting files used to setup your local development environment. I received this file from Lisa and placed it on my local OneDrive at this location:
> VA JLV-CV / 04 Test / Setup /

This folder contains four zip files and two txt files. The zip files are further described in the sections below.  

The two text files are:
> README.txt - Lisa's dev notes and instructions for the Setup folder
> service.txt - XRay auth file who's location is set in project.properties

I placed service.txt in this location:
> c:\work\test-automation\

I edited jlv-test-automation/src/main/resources/project.properties as follows:
> XRAY_AUTH_FILE=c:/work/test-automation/service.txt

**Note:** Lisa's instructions show a slightly different path to project.properties.

#### .settings folder
Unzipped and placed `sa.key` in the following location:
> jlv-test-automation\src\main\resources\\.settings\

#### Common folder
Created new folder:
> c:\Automation\

Unzipped Common.zip into Automation, resulting in new folder with files:
> c:\Automation\Common\s_jlvsv.jks
> c:\Automation\Common\service.auth

#### drivers folder
Unzipped drivers.zip into Automation, resulting in new folder with files:
> c:\Automation\drivers\chromedriver.exe
> c:\Automation\drivers\IEDriverServer.exe
> c:\Automation\drivers\msedgedriver.exe

**Note:** These are the drivers for the current versions of the respective browsers; the driver and the browser versions must be consistent.

Drivers can be found at:

- https://chromedriver.chromium.org/downloads
- https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/#downloads   

#### pii folder
Unzipped pii.zip into:
> jlv-test-automation\src\test\datafiles\Users.xml
> jlv-test-automation\src\test\datafiles\zipPass

Edited Users.xml to contain my Access and Verify codes.

Per Lisa's README.md instructions, do not commit the updated version of Users.xml to source control.

### Running Test Scripts
Test scripts are run from within IntelliJ. Ideally, in my opinion, we will create the ability to run test scripts directly from the Windows command line.

## Luis Medina Setup
Luis was detailed to the JLV project for a period of time to assist with standing up our automated testing framework in a way that can be used within the VAEC cloud environment. In doing so, he consolidated the two test automation repositories that Lisa has been working with into a new, single repository:

Repository | Description
---------- | -----------
jlv-test-harness | Consolidation of prior two repo's into single repo to build, deploy, and run within AWS cloud

To become acclimated with this, I downloaded a zip of this repo to my local GFE and ran through the setup steps. My copy of the repo is located at:  

> /c/work/swdev/jlv-test-harness-20211025

To try and keep things simple and reduce the magic that IntelliJ performs, I am using VSC to view the source code, target, etc. I believe I have VSC configured so that it doesn't do anythin intrusive when I open the Java project folder.
