# Python Language Reference Guide
Things I always forget

## Introduction
Python is an object-oriented scripting language released publicly in 1991. Product features are captured and proposed in the form of a `Python Enhancement Proposal (PEP)`. Because the PEPs are maintained as text files in a versioned repository, their revision history is the historical record of the feature proposal. For more information, see: https://www.python.org/dev/peps/.

## Getting Started
Visit https://www.python.org/ for the latest info on Python.

The current version of Python for MacOS is 3.10.0.

My MBP 16 2019 currently has several versions of Python installed:
```bash
/usr/local/bin/

% python --version
Python 2.7.18

% python2 --version
Python 2.7.18

% python3 --version
Python 3.8.9
```

### Install Python
Deitel recommends the use of the Anaconda Python distribution. The latest version for MacOS, per the Anaconda website, is Python 3.9. Since I'm learning Python via the Deitel book, I'll follow this advice.
> https://www.anaconda.com/downlaod/

After the Anaconda install, we get the following Python version:
```bash
python --version
Python 3.9.7

python3 --version
Python 3.9.7
```

### Package Managers
For the Deitel work, we will be using two package managers:
 - conda package manager
 - pip

Packages contain the files required to install a given Python library or tool.

### Supporting Tools
Installed several additional tools, per Deitel, including Prospector static code analysis tool and some visualizaton tools. Also installed latest version of Node.js from the nodejs.org website.
```bash
% pip install prospector
% conda install -c conda-gorge ipympl
% jupyter labextension install @jupyter-widgets/jupyterlab-manager
& jupyter labextension install jupyter-matplotlib
```

### Twitter Developer Acccount
Registered for a twitter developer account via:
https://developer.twitter.com/en/apply-for-access

### Start Python Interpreter
Invoke the Python interpreter:
```bash
% python3
```

### End Python Interpreter
Typing an end-of-file character (Control-D on Unix, Control-Z on Windows) at the primary prompt causes the interpreter to exit with a zero exit status.  

If that doesn’t work, you can exit the interpreter by typing the following command: quit().

## IPython Interactive Mode
Start IPython from the Terminal command line, enter:
```bash
% ipython
```

Exit IPython interactive mode via one of the following:
```bash
% exit
% <control> + d  (this displays prompt to confirm exit)
% <control> + d  (type this twice to exit without confirmation prompt)
```

