# Chuck's Git & GitHub Reference Guide
Things I always forget

## Getting Started

### Set up Git
Set up basic information about yourself and how you want to work with Git.  

```bash
git config --global user.name "Chuck Sylvester"
git config --global user.email charles.sylvester@va.gov
git config --global color.ui true
```

To display your Git settings:

```bash
git config --list
git config --local --list
git config --global --list
```

### Clone remote repository to local instance
I have two-factor authentication enabled and a Personal Access Token, so I need to provide a more verbose version of the clone command. I typically use the HTTPS option for cloning.

After copying the desired repo URL to the clipboard, you need to tweak it after pasting into termninal command line. A while back, I did this by including both the username and personal access token as part of the clone command. After doing this for an initial repository, I only needed to include username for subsequent clones.

However, I recently tried this approach and it did not work. So instead, I cloned with only my username added, and then when prompted for password, I pasted in my personal access token string.

For example, to clone the department-of-veterans-affairs repository, jlv-cv-repo, I issue the following commands:

```bash
cd c:\work\swdev\va
git clone chuck-sylvester@github.com/department-of-veterans-affairs/jlv-cv-repo.git
```

Then, when prompted for password, paste in your personal access token string and hit enter. It may not be necessary to provide your personal access token string upon subsequent clone statements.

I've recently had some trouble with this approach on my GFE. Instead, I've had to issue a variation of the clone commnad that uses my Personal Access Token as part of the URL. This worked when I cloned the two JLV Test Automation repositories.

```bash
cd c:\work\swdev\va
git clone https://<personal-access-token>@github.com/department-of-veterans-affairs/jlv-cv-automation-framework.git
git clone https://<personal-access-token>@github.com/department-of-veterans-affairs/jlv-test-automation.git
```

### Working with the .gitignore file
Use # symbol for comments.
MacOS file type to ignore: `.DS_Store`

### Working with Branches
By default, when a new repository is initialized, it creates branch "master" (or "main").

Generally, the JLV/CV repositories are set up such that developers cannot work directly against the main branch. Instead, they create their own branch(es) related to their specific work. Then, their updates can be merged into the long running development branch as part of a Pull Request (PR).

To create a new branch, e.g. "my-branch", use the git command:

```bash
git branch my-branch
```

This creates a new pointer to the same commit you're currently on. Git keeps a special pointer called HEAD, which is a pointer to the local branch you're currently on. In this case, if you are currently on "main", the git branch command only creates a new branch. It did not switch to that branch.

You can see this by running a git log command that shows you where the branch pointers are pointing. This option is called --decorate.

```bash
git log --oneline --decorate
```

To switch to an existing branch, run the git checkout command, e.g.:

```bash
git checkout my-branch
```

This moves the HEAD to poit to the my-branch branch. Any changes made will only be visible in my-branch.

You can use a shortcut command to create and switch to a new branch:

```bash
git checkout -b my-branch
```

To switch back to the main branch:

```bash
git checkout main
```

The git branch command does more than just create and delete branches. If you run it with no arguments, you get a simple listing of your current branches.

```bash
git branch
```

To see the last commit on each branch, run the following:

```bash
git branch -v
```

To see a list of all branches in the remote repository, use either of these commands:

```bash
git branch --all
git branch -r
```

The  --merged and --unmerged options can filter this list to branches you have or have not yet merged into the branch you're currently on.

```bash
git branch --merged
git branch --no-merged
```

To delete a local branch, run the following:

```bash
git branch -d <local-branch>
```

To push the current branch and set the remote as upstream, use one of the following:

```bash
git push --set-upstream origin cps-development
git push -u origin cps-development
```

### Sync Local Branch with Specific Remote Branch


## GitFlow (for better or worse)
This workflow uses three types of branches:

Branch Name | Description
----------- | -----------
main    | long-lived and contains each of the major product versions
develop | long-lived and serves as the integraton and build branch
feature | multiple, short-lived branches used by individual developers

The first step is to complement the default ***main*** with a ***develop*** branch. A simple way to do this is for one developer to create an empty develop branch locally and push it to the server:

```bash
git branch develop
git push -u origin develop
```

This branch will contain the complete history of the project, whereas main will contain an abridged version. Other developers should now clone the central repository and create a tracking branch for develop.

**Feature Branches**
Each new feature should reside in its own branch, which can be pushed to the central repository for backup/collaboration. But, instead of branching off of main, feature branches use develop as their parent branch. When a feature is complete, it gets merged back into develop. Features should never interact directly with main.

**Finishing a feature branch**
When you’re done with the development work on the feature, the next step is to merge the feature_branch into develop.

```bash
git checkout develop
git merge feature_branch
```

We preceed merges with a Pull Request (PR) and approval. Then, we typically use GitHub to perform the merger from the feature branch into the develop branch.