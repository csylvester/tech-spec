# tech-spec

Online Technical Reference

## Description
Tech Spec is an online reference guide for the technologies we use. It proivdes a quick quide for the major technology features and how we use them in a standard and consistent way.

## Installation
This is a very simple project that consists of markdown files for each topic. The repo is not compiled or deployed anywhere. Rather, it can be accessed via GitLab online or via a local copy. An app that I particularly like for viewing MD files on my MacBook is *Marked 2*.

## Authors and acknowledgment
This project was created by C. Sylvester, and does not currently have any other contributors.

## Project status
This project is under active development. The technology focus changes over time, but the content still serves as a helpful reference when returning to a topic after a period of time away.
