# VistA Sites

### CHEY260 (IPO4)
Key | Value | Note
--- | ----- | ----
Access Code | 0979HSP |
Verify Code | aM0unt#01 | Old
Verify Code | bm0unt#02 |Old
Verify Code | cm0unt#03 | New

Host Name: vhaispdbschy13.vha.med.va.gov
Host PW: chey260

### NHMT7 (IPO5)
Key | Value | Note
--- | ----- | ----
Access Code | 9762TTU
Verify Code | CPS#1993# | Old
Verify Code | bm0unt#02 | New

### Change Expired Verify Code
Login Message: VERIFY CODE must be changed before continued use.

WHY?: Your CPRS or CAPRI Verify code has expired.

FIX IT: Open CPRS, VistA, or CAPRI (CAPRI-Claims users)
If prompted for a PIV card certificate by CPRS, click Cancel
You are prompted to create a new Verify code
Once your Verify code has been changed for CPRS or CAPRI, relaunch JLV, which recognizes the new code immediately

### Exit VisA Session
Within VistA: ^ to exit session