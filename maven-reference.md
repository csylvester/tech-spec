# Apache Maven Reference
Chuck's guide to installation and use of Maven

## Download Maven for Mac
Refer to: https://maven.apache.org/

Download and unzip the binary zip archive to a folder, e.g. Downloads:
> apache-maven-3.8.3-bin.zip

Copy (or move) the resulting top-level folder to:
> /opt

This results in:
> /opt/apache-maven-3.8.3

Update ~/.zshrc to include path to maven bin location:
```bash
# Maven config info
export PATH=/opt/apache-maven-3.8.3/bin:$PATH
```
Close the current terminal session and start a new session.
Verify the Maven install via:

```bash
mvn -v
```

## First Maven Project
I am using the simple-test repository

Create a new project at the top level of the repository using Apache Maven command:

```bash
cd ~/swdev/va/simple-test

mvn archetype:generate -DgroupId=com.csylvester.sample -DartifactId=sample-test -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```

This will create the full Maven-based directory structure, along with sample Java HelloWorld main and test programs.

To complile the yahoo-test-app project, run the following Maven command:
```bash
cd ~/swdev/va/simple-test/yahoo-test-app
mvn verify
```

To run the main method within the sample HelloWorld app:
```bash
cd ~/swdev/va/simple-test/yahoo-test-app
java -cp target/classes com.csylvester.yahoo.App
```

The Maven package command may also be appropriate for compiling/building the app:
```bash
cd ~/swdev/va/simple-test/yahoo-test-app
mvn package
```
Running the created app after the package command may involve referencing a package with the -cp option. Will take a closer look at this soon.

To run the main method within the sample TestApp app:
```bash
Actually, I am not yet sure how to run it... stay tuned.
```

