# Chuck's Guide to Selenium WebDriver

## Download and Install Selenium
Selenium is provided as a set of JAR files.

https://www.selenium.dev/downloads/

Extract the .zip file you downloaded and add the Selenium Java bindings (a JAR file) and add all the dependent libraries (JAR files in the /libs folder) to your classpath.